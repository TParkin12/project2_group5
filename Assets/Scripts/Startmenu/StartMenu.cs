﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
enum Menu_Button
{
    Menu_Button_1,
    Menu_Button_2,
    Menu_Button_3
};


public class StartMenu : MonoBehaviour
{
   [SerializeField] private Menu_Button ButtonList;
   [SerializeField] private RawImage Start_Menu;
   [SerializeField] private RawImage Controls_Menu;
   [SerializeField] private RawImage Exit_Menu;
   [SerializeField] private Animator Start_Animaton;
    [SerializeField] private GameObject Controls_Text;
    private int Count;
    private AudioManager AudioMan;

    // Start is called before the first frame update
    void Start()
    {
        Start_Menu = GameObject.Find("Start_Game_HighLight").GetComponent<RawImage>();
        Controls_Menu = GameObject.Find("Controls_HighLight").GetComponent<RawImage>();
        Exit_Menu = GameObject.Find("Exit_HighLight").GetComponent<RawImage>();
        Start_Animaton = GameObject.Find("Start_Animation").GetComponent<Animator>();
        Controls_Text = GameObject.Find("Controls_Text");
        Controls_Text.SetActive(false);
        ButtonList = Menu_Button.Menu_Button_1;
        Count = 1;
        AudioMan = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Count = Mathf.Clamp(Count, 1, 3);

        GetInput();

        EnableButtons();
    }
    
    void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            Count++;

            InputResponse();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            Count--;

            InputResponse();
        }

        if(Input.GetKeyDown(KeyCode.Return) && ButtonList == Menu_Button.Menu_Button_1)
        {
            Start_Animaton.SetBool("Begin", true);
           
        }

        if (Input.GetKeyDown(KeyCode.Return) && ButtonList == Menu_Button.Menu_Button_2)
        {
            if (Controls_Text.activeSelf == false)
                Controls_Text.SetActive(true);
            else
                Controls_Text.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Return) && ButtonList == Menu_Button.Menu_Button_3)
        {
            Application.Quit();
        }
    }
    
    void Game_Start()
    {
        AudioMan.StopAllAudio();
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    void EnableButtons()
    {
        switch (ButtonList)
        {
            case Menu_Button.Menu_Button_1:
                Start_Menu.enabled = true;
                Controls_Menu.enabled = false;
                Exit_Menu.enabled = false;
                break;
            case Menu_Button.Menu_Button_2:
                Start_Menu.enabled = false;
                Controls_Menu.enabled = true;
                Exit_Menu.enabled = false;
                break;
            case Menu_Button.Menu_Button_3:
                Start_Menu.enabled = false;
                Controls_Menu.enabled = false;
                Exit_Menu.enabled = true;
                break;
            default:
                Start_Menu.enabled = true;
                break;
        }
    }

    void InputResponse()
    {
        if (Count == 1)
        {
            ButtonList = Menu_Button.Menu_Button_1;
        }

        if (Count == 2)
        {
            ButtonList = Menu_Button.Menu_Button_2;
        }

        if (Count == 3)
        {
            ButtonList = Menu_Button.Menu_Button_3; 
        } 
    }
}
