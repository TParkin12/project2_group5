﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Key : MonoBehaviour
{

    enum Key_Name{
        Gas_Key,
        Bathroom_Key_Start,
        Bathroom_Key_Dummy,
        Bathroom_Key_Real,
        Cafe_Key
    };


    [SerializeField] private TMP_Text Key_Text;
    [SerializeField] private GameObject Keys_; 
    private bool PuzzleStopped = false;
    private bool PuzzleStarted = false;
    [SerializeField] private Key_Name KEY;

    

    private void Start()
    {
        Key_Text = GameObject.Find("Tooltip_Text").GetComponent<TMP_Text>();
        Keys_ = GameObject.Find("Bathroom_Keys");

        if (KEY == Key_Name.Bathroom_Key_Start)
        {
            Keys_.SetActive(false);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        

        if ( other.gameObject.name == "Player")
        {
            

            if (Input.GetKeyDown(KeyCode.F))
            {
                switch(KEY)
                {
                    case Key_Name.Bathroom_Key_Dummy:
                       Key_Text.text = "Not the key";
                       StartCoroutine(Show_ToolTip());
                        break;

                    case Key_Name.Bathroom_Key_Real:
                        FindObjectOfType<BathroomPuzzle>().StopWater(true);
                        PuzzleStopped = true;
                        Add_Key_To_Count();
                        break;

                    case Key_Name.Bathroom_Key_Start:
                        PuzzleStarted = true;
                        Keys_.SetActive(true);
                        FindObjectOfType<BathroomPuzzle>().StopWater(false);
                        Key_Text.text = "What is this...";
                        StartCoroutine(Show_ToolTip());
                        break;

                    case Key_Name.Gas_Key:
                        FindObjectOfType<GasRoomPuzzle>().StopGas(true);
                        PuzzleStopped = true;
                        Add_Key_To_Count();
                        break;

                    case Key_Name.Cafe_Key:
                        PuzzleStarted = true;
                        FindObjectOfType<MazePuzzle>().SpawnMaze();
                        Add_Key_To_Count();
                        break;
                }                
                Key_Text.enabled = (false);
                Key_Text.text = "Text_Broken_Key";
                this.gameObject.SetActive(false);
            }

            if(!PuzzleStopped && !PuzzleStarted)
            {
                Key_Text.enabled = (true);
                Key_Text.text = "Press F To Grab Key"; 
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Key_Text.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        PuzzleStopped = false;
    }

    static private void Add_Key_To_Count()
    {
        FindObjectOfType<Nurse_Office_Lock>().IncrementKey();
    }

    IEnumerator Show_ToolTip()
    {
        Key_Text.enabled = true;
        yield return new WaitForSeconds(2);
        Key_Text.enabled = false;
    }
}
