﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using TMPro;
public class GasRoomPuzzle : MonoBehaviour
{
    private DoorScript Doors;
    private DensityVolume DV;

    private TMP_Text Text;
    [SerializeField] private AudioManager AudioMan;

    [SerializeField] private float Time;
    [SerializeField] private bool CountdownBegan;
    private bool CountdownStopped;

    private void Awake()
    {
        Doors = GameObject.Find("Med_Room_Doors").GetComponentInChildren<DoorScript>();
        DV = GameObject.Find("MedRoom_Area").GetComponent<DensityVolume>();
        Text = GameObject.Find("Tooltip_Text").GetComponent<TMP_Text>();
        Text.enabled = false;
        DV.parameters.distanceFadeEnd = Time;
        Time = 0.0f;
        AudioMan = FindObjectOfType<AudioManager>(); 
        CountdownBegan = false;
        CountdownStopped = false;
    }

    private void Update()
    {
       DV.parameters.distanceFadeEnd = Time / 10;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == "Player" && !CountdownStopped)
        {
            Doors.Set_LockDoors(true);
            startTimer();
        }
        if(CountdownStopped)
        {
          
            Doors.Set_LockDoors(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player" )
        {
            AudioMan.StopAllAudio();
            AudioMan.Play("Battle");
            StartCoroutine(SaySpeech());
            
        }
    }



    public void StopGas(bool Stop)
    {
        if(Stop)
        {
            CountdownStopped = true;
            FindObjectOfType<Inventory>().TakeDamage(false);
            AudioMan.StopAllAudio();
            AudioMan.Play("BGM");
        }
    }
    IEnumerator Timer()
    {
        CountdownBegan = true;
        yield return new WaitForSeconds(1);
        FindObjectOfType<Inventory>().TakeDamage(true);
        Time += 1;
        CountdownBegan = false;
    }

    IEnumerator SaySpeech()
    {
        Text.enabled = (true);
        Text.text = "The door just locked...I-is that Gas, I better find a way out.";
        yield return new WaitForSeconds(5);
        Text.enabled = (false);
    }
    private void startTimer()
    {
        if (Time < 100 && CountdownBegan == false)
        StartCoroutine(Timer());
    }
}
