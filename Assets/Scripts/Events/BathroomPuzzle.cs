﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using TMPro;
public class BathroomPuzzle : MonoBehaviour
{
    private DoorScript Doors;
    private Light Bathroom_Light;
    private TMP_Text Text;
    private AudioManager AudioMan;
    private GameObject Water;

    [SerializeField] private float Time;
    [SerializeField] private bool CountdownBegan;
    [SerializeField] private bool CountdownStopped;
    private float WaterLevel;
    private Vector3 WaterBase;

    private void Awake()
    {
        Doors = GameObject.Find("Bathroom_Door").GetComponentInChildren<DoorScript>();
        Text = GameObject.Find("Tooltip_Text").GetComponent<TMP_Text>();
        Bathroom_Light = GameObject.Find("Bathroom(Light)").GetComponent<Light>();
        AudioMan = FindObjectOfType<AudioManager>();
        Water = GameObject.Find("Water_Plane");
        Text.enabled = false;
        CountdownBegan = false;
        CountdownStopped = true;
        Time = 0.0f;
        WaterLevel = -2.4f;
        WaterLevel = Mathf.Clamp(WaterLevel, -2.1f, 0.7f);
        Water.transform.localPosition = new Vector3(Water.transform.localPosition.x, WaterLevel, Water.transform.localPosition.z);

        WaterBase = Water.transform.localPosition;
    }

    private void Update()
    {
        if(!CountdownStopped)
            Water.transform.localPosition = new Vector3(Water.transform.localPosition.x, WaterLevel, Water.transform.localPosition.z);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player" && !CountdownStopped)
        {
            Doors.Set_LockDoors(true);
            startTimer();
        }
        if (CountdownStopped)
        {
           
            Doors.Set_LockDoors(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            AudioMan.StopAllAudio();
            AudioMan.Play("Battle");
            StartCoroutine(SaySpeech());
        }
    }




    public void StopWater(bool Stop)
    {
        if (Stop)
        {
            CountdownStopped = true;
            AudioMan.StopAllAudio();
            AudioMan.Play("BGM");
            Water.transform.localPosition = Vector3.Lerp(Water.transform.localPosition, WaterBase, 2.0f);
        }
        if(!Stop)
        {
            CountdownStopped = false;
        }
    }
    IEnumerator Timer()
    {
        CountdownBegan = true;
        yield return new WaitForSeconds(1);
        FindObjectOfType<Inventory>().TakeDamage(true);
        FlickerLight();
        if(WaterLevel >= 0.5)
        {
            FindObjectOfType<Inventory>().KillPlayer(true);
        }
        WaterLevel += 0.1f;
        Time += 1;
        CountdownBegan = false;
    }

    IEnumerator SaySpeech()
    {
        Text.enabled = (true);
        Text.text = "I gotta find that key";
        yield return new WaitForSeconds(5);
        Text.enabled = (false);
    }
    private void startTimer()
    {
        if (Time < 100 && CountdownBegan == false)
            StartCoroutine(Timer());
    }


    private void FlickerLight()
    {
        Bathroom_Light.intensity = Random.Range(0, 0.15f);
    }
}
