﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public GameObject TriggerBox;

    private void OnTriggerStay(Collider other)
    {
        GetComponentInParent<Camera>().enabled = true;
    }
    private void OnTriggerExit(Collider other)
    {
        GetComponentInParent<Camera>().enabled = false;
    }

    private void Start()
    {
        GetComponentInParent<Camera>().enabled = false;
    }
} 
