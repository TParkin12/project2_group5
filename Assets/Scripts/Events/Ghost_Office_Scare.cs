﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
public class Ghost_Office_Scare : MonoBehaviour
{
    private TMP_Text Text;
    private TMP_Text EndText;
    [SerializeField] private VideoPlayer GhostSpook;
    [SerializeField] private RawImage FadeOut;
    [SerializeField] private VideoClip Ghost_Spook_Clip;

    private bool Game_End = false;
    private void Start()
    {
        Text = GameObject.Find("Tooltip_Text").GetComponent<TMP_Text>();
        EndText = GameObject.Find("End_Text").GetComponent<TMP_Text>();
        EndText.enabled = false;
        GhostSpook = GameObject.Find("VideoPlayer").GetComponent<VideoPlayer>();
      
        FadeOut = GameObject.Find("fadeoutScreen").GetComponent<RawImage>();
        
        GhostSpook.Stop();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            GhostSpook.Stop();

            FindObjectOfType<AudioManager>().StopAllAudio();
            FindObjectOfType<AudioManager>().Play("Safety");
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
            Text.enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
        GhostSpook.playOnAwake = false;
        if(other.gameObject.name == "Player")
        {
            Text.enabled = true;
            Text.text = "Press F to Interact";
            GhostSpook.clip = Ghost_Spook_Clip;
          

            if (Input.GetKeyDown(KeyCode.F))
            {
                Text.enabled = false;
                StartCoroutine(Fade());
            }

            if (Game_End && Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }

    IEnumerator Fade()
    {
        FindObjectOfType<Movement>().CanMove = false;
        
        FadeOut.canvasRenderer.SetAlpha(0.0f);
        FadeOut.enabled = true;
        FadeOut.CrossFadeAlpha(1, 2, false); // 0 - 1 How visiable , 2 = Timer, ingoreTimeScale
        
        yield return new WaitForSeconds(5);

        GhostSpook.Play();

        yield return new WaitForSeconds(3);


        Game_End = true;
        EndText.enabled = true;
        EndText.text = "Thank You for Playing Please Press ESC to Quit";
    }
}
