﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nurse_Office_Lock : MonoBehaviour
{
    [SerializeField] private int Keys_Collected;
    [SerializeField]private bool Door_Unlocked;

    public void IncrementKey()
    {
        Keys_Collected++;
    }

    private void Update()
    {
        if(Keys_Collected == 3)
        {
            Door_Unlocked = true;
        }
    }

    public bool Get_Lock_State()
    {
        return Door_Unlocked;
    }
}
