﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using TMPro;

public class MazePuzzle : MonoBehaviour
{
    private DoorScript Doors;
    private TMP_Text Text;
    private AudioManager AudioMan;

    [Header("GameObjects")]
    [SerializeField] private GameObject Maze;
     private GameObject Pre_Maze;
    [SerializeField] private GameObject Camera_Pre_Maze;
    [SerializeField] private GameObject Camera_Maze;
    private GameObject Lights; 


    private void Awake()
    {
        Doors = GameObject.Find("Maze_Door").GetComponentInChildren<DoorScript>();
        Text = GameObject.Find("Tooltip_Text").GetComponent<TMP_Text>();
        AudioMan = FindObjectOfType<AudioManager>();
        Pre_Maze = GameObject.Find("Furniture");
        Lights = GameObject.Find("Maze_Lights");
    }

    public void SpawnMaze()
    {
        Pre_Maze.SetActive(false);
        Maze.SetActive(true);
        Camera_Pre_Maze.SetActive(false);
        Camera_Maze.SetActive(true);
        Lights.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        AudioMan.StopAllAudio();
        AudioMan.Play("BGM");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            AudioMan.StopAllAudio();
            AudioMan.Play("Battle");
            StartCoroutine(SaySpeech());
        }
    }

    

    IEnumerator SaySpeech()
    {
        Text.enabled = (true);
        Text.text = "What a mess";
        yield return new WaitForSeconds(5);
        Text.enabled = (false);
    }

}
