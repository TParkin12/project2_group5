﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchVision : MonoBehaviour
{
    private GameObject Decal;

    private void OnTriggerStay(Collider other)
    {
      

        if(other.gameObject.layer == 8)
        {
            Decal = other.gameObject;
            Decal.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            Decal.GetComponent<MeshRenderer>().enabled = false;
        }
    }

}
