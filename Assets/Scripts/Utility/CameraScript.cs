﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject TriggerBox;
    [SerializeField] private bool isStatic;
    private bool CanRotate;
    private GameObject Target;
    private Quaternion CameraRotation;
    [SerializeField] private float RotationSpeed;
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            GetComponentInChildren<Camera>().enabled = true;
            CanRotate = true;
        }
       
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            GetComponentInChildren<Camera>().enabled = false;
            CanRotate = false;
        }
           
    }

    private void Start()
    {
        GetComponentInChildren<Camera>().enabled = false;
        CameraRotation = GetComponentInChildren<Camera>().transform.rotation;
        Target = GameObject.Find("Player");
        CanRotate = false;
    }

    private void Update()
    {
        if (!isStatic && CanRotate)
        {
            Vector3 Direction = Target.transform.position - GetComponentInChildren<Camera>().transform.position;
            GetComponentInChildren<Camera>().transform.rotation = Quaternion.Lerp(CameraRotation, Quaternion.LookRotation(Direction), RotationSpeed * Time.deltaTime);
        }
    }
}