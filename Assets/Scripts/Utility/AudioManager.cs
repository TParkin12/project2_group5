﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    public static AudioManager instance;

    void Awake()
    {

        foreach (Sound s in sounds)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.clip;

            s.Source.volume = s.volume;
            s.Source.pitch = s.pitch;
        }
    }


    private void Start()
    {
        foreach (Sound s in sounds)
        {
            if (s.PlayAtWake == true)
            {
                s.Source.Play();
            }

            if (s.loop == true)
            {
                s.Source.loop = true;
            }
            
        }
       
        
    }

    public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound:" + name + "was not located");
            return;
        }
            s.Source.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            Debug.LogWarning("Sound:" + name + "was not located");
            return;
        }
   

        s.Source.Stop();
    }


    public void StopAllAudio()
    {
        foreach (Sound s in sounds)
        {
            s.Source.Stop();
        }
    }
}

