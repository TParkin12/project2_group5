﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchSystem : MonoBehaviour
{
    public bool isCameraEnabled;
    public GameObject PreviousCamera;
    public GameObject Camera;

    // Update is called once per frame
    void Update()
    {



        if(isCameraEnabled)
        {
            PreviousCamera.SetActive(false);
            Camera.SetActive(true);
        }
        else
        {
            
        }
         
    }
}
