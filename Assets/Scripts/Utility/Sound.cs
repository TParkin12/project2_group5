﻿using UnityEngine.Audio;
using UnityEngine;


[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;
    [Range(0f,1f)]// Change the Volume
    public float volume;
    [Range(1f, 3f)] // Change the Pitch
    public float pitch;

    public bool loop; //Loop Audio
    public bool PlayAtWake; // Play audio on start

    [HideInInspector]
    public AudioSource Source;
}
