﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine;
using TMPro;
public class DoorScript : MonoBehaviour
{
    private GameObject Player;
    [SerializeField] private GameObject StartPoint;
    [SerializeField] private GameObject EndPoint;
    [SerializeField] private RawImage PlayDoor;
    [SerializeField] private TMP_Text DoorText; 
    [SerializeField] private VideoPlayer DoorAnimation;
    static private bool DoorLock;
    [SerializeField]private bool IsOfficeDoor;


    private void Start()
    {
        Player  = GameObject.Find("Player");
        PlayDoor = GameObject.Find("FadeOut").GetComponentInChildren<RawImage>();
        DoorAnimation = GameObject.Find("VideoPlayer").GetComponent<VideoPlayer>();
        DoorText = GameObject.Find("Door_Text").GetComponent<TMP_Text>();
        DoorAnimation.Stop();
        PlayDoor.enabled = false;
        //FadeOut.canvasRenderer.SetAlpha(0.0f); 
        DoorLock = false;
        DoorText.enabled = (false);
    }
    private void OnTriggerExit(Collider other)
    {
        DoorText.enabled = (false);
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == "Player" && !DoorLock)
        {
            DoorText.enabled = (true);
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (!IsOfficeDoor)
                {
                    DoorText.text = "Press F To Open Door";
                    StartCoroutine(WaitForFadeOut());
                }
                   
                else if (IsOfficeDoor && FindObjectOfType<Nurse_Office_Lock>().Get_Lock_State())
                {
                    StartCoroutine(WaitForFadeOut());
                }
                else if (IsOfficeDoor && !FindObjectOfType<Nurse_Office_Lock>().Get_Lock_State())
                    DoorText.text = "You must have all three keys";

            }

           
        }
       
    }
    
    

    IEnumerator WaitForFadeOut()
    {
        //PlayDoor.CrossFadeAlpha(1, 2, false); // 0 - 1 How visiable , 2 = Timer, ingoreTimeScale
        PlayDoor.enabled = true;
        DoorAnimation.Play();
        Player.GetComponent<Movement>().CanMove = false;
        yield return new WaitForSeconds(5.0f);
        PlayDoor.enabled = false;
        Player.GetComponent<Movement>().CanMove = true;
        DoorAnimation.Stop();
        //PlayDoor.CrossFadeAlpha(0, 1, false);
        Player.transform.position = EndPoint.transform.position;
    }

    public void Set_LockDoors(bool Lock)
    {
        DoorLock = Lock;
    }

}

