﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
public class Movement : MonoBehaviour
{
    [Header("References")]
    private GameObject mPlayer;
    private Animator mAnimate;

    [Header("Flags")]
    public bool isMoving;
    public bool isRotating;
    public bool CanMove;

    [Header("Movement Vector")]
    private float Horizontal;
    private float Vertical;

    [Header("Designer Controls")]
    public float SpeedMulitplierVertical;
    public float SpeedMulitplierHorizontal;

    [Header("Coder Controls")]
    private float SpeedSlowedDown;

    private void Start()
    {
        mPlayer = GameObject.Find("Player");
        mAnimate = mPlayer.GetComponentInChildren<Animator>();
        CanMove = true;    
    }

    void Update()
    {
        if(CanMove)
        {
            if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
            {

                isMoving = true;
                Horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * SpeedMulitplierHorizontal;
                mPlayer.transform.Rotate(0, Horizontal, 0);
                Vertical = Input.GetAxis("Vertical") * Time.deltaTime * SpeedMulitplierVertical;
                if (isRotating)
                {
                    Vertical = Input.GetAxis("Vertical") * Time.deltaTime * SpeedSlowedDown;
                    mPlayer.transform.Translate(0, 0, Vertical);
                }
                else
                    mPlayer.transform.Translate(0, 0, Vertical);

                if (Input.GetButton("Horizontal") && Input.GetButton("Vertical"))
                {
                    isRotating = true;
                    SpeedSlowedDown = SpeedMulitplierVertical / 1.5f;
                }
            }
            else
            {
                isMoving = false;
                isRotating = false;
                mAnimate.SetBool("IsWalking", false);
            }

            if(isMoving)
            {
                mAnimate.SetBool("IsWalking", true);
            }

        }
    }
}
