﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPickup : MonoBehaviour
{
    private GameObject UiCam;
    private GameObject Player;
    private bool ActivateUICamera;


    private void Start()
    {
        Player = GameObject.Find("Player");
        UiCam = GameObject.Find("UICAMERA");
        UiCam.GetComponent<Camera>().enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
       
        if (Input.GetKeyDown(KeyCode.F) && ActivateUICamera == false)
        {
            UiCam.GetComponent<Camera>().enabled = true;
            ActivateUICamera = true;
            Player.GetComponent<Movement>().CanMove = false;
        }
        else if (Input.GetKeyDown(KeyCode.F) && ActivateUICamera == true)
        {
            UiCam.GetComponent<Camera>().enabled = false;
            ActivateUICamera = false;
            Player.GetComponent<Movement>().CanMove = true;
        }
        
    }
}
