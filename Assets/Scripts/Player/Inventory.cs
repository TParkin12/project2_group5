﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Inventory : MonoBehaviour
{
   public bool InvetoryIsOpen;
   private GameObject UICam;
   private Image UIHealthBar;
   private Image UI_Inventory;
  [SerializeField] public float health;
    private void Start()
    {
        UIHealthBar = GameObject.Find("FillHealthBar").GetComponent<Image>();
        UI_Inventory = GameObject.Find("InventoryScreen").GetComponent<Image>();
        UI_Inventory.enabled = (false);
        InvetoryIsOpen = false;
        health = 100;
        TakeDamage(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && InvetoryIsOpen == false)
        {
            InvetoryIsOpen = true;
            UI_Inventory.enabled = (true);
        }
        else if( (Input.GetKeyDown(KeyCode.E) && InvetoryIsOpen == true))
        {
            InvetoryIsOpen = false;
            UI_Inventory.enabled = (false);
        }
        if (InvetoryIsOpen)
        {
            if(Input.GetKey(KeyCode.H))
            {
                TakeDamage(true);
            }
        }
       
        UIHealthBar.fillAmount = health / 100;
    }


    public void TakeDamage(bool Active)
    {
        if(Active)
            health--;
    }

    public void KillPlayer(bool Kill)
    {
        if(Kill)
        {
            health = 0;
            FindObjectOfType<Movement>().CanMove = false;
            StartCoroutine(DeathScreen());
        }
    }

    IEnumerator DeathScreen()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
