﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInspector : MonoBehaviour
{
    public GameObject InspectedObject;
    private GameObject UICam;
    private float RotationAmount;
    private void Start()
    {
        
        UICam = GameObject.Find("UICAMERA");
        RotationAmount = 0.0f;
    }
    private void Update()
    {
        if (UICam.GetComponent<Camera>().enabled == true)
        {
            if(Input.GetKey(KeyCode.W))
            {
                InspectedObject.transform.localScale += new Vector3(1,1,1);
            }
            if (Input.GetKey(KeyCode.S))
            {
                InspectedObject.transform.localScale -= new Vector3(1, 1, 1);
            }
            if (Input.GetKey(KeyCode.A))
            {
                RotationAmount++;
                InspectedObject.transform.localRotation = Quaternion.Euler(RotationAmount, 0, RotationAmount);
            }
            if (Input.GetKey(KeyCode.D))
            {
                RotationAmount--;
                InspectedObject.transform.localRotation = Quaternion.Euler(RotationAmount,0, RotationAmount);
            }
        }    
    }
}
